import { Server } from 'http';
import socketio from 'socket.io';
import { ServerEvents } from 'types';
import { decodeJWT } from '../controller/jwt';
import { getRoomById } from '../controller/room';
import { EventHandlers } from '../types';

class Socket {
  private io?: SocketIO.Server;

  init(server: Server, events: EventHandlers) {
    this.io = socketio(server);

    const jwtMiddleware = async (jwt?: string) => {
      if (!jwt) throw Error('not authorized');
      const jwtData = decodeJWT(jwt);
      if (!jwtData) throw Error('not authorized');

      const room = await getRoomById(jwtData.roomId);

      return {
        room,
        jwtData,
      };
    };

    this.io.on('connection', socket => {
      Object.entries({
        ...events.public,
        ...events.private,
      }).forEach(([event, handler]) => {
        socket.on(event, async (jwt, payload, cb) => {
          try {
            const { room, jwtData } = await jwtMiddleware(jwt).catch(e =>
              // only Error for private events
              Object.keys(events.public).includes(event)
                ? Promise.resolve({} as any)
                : Promise.reject(e)
            );

            cb(
              undefined,
              await handler({
                jwtData,
                room,
                payload,
                socket,
              })
            );
          } catch (e) {
            console.error(e);
            cb(e);
            socket.error(`something went wrong \n${e}`);
          }
        });
      });
    });
  }

  emitToRoom<E extends keyof ServerEvents>(
    room: string,
    event: E,
    payload: ServerEvents[E]
  ) {
    this.io?.to(room).emit(event, payload);
  }
}

export default new Socket();

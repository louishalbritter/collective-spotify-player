import { schedule } from 'node-cron';
import Credentials from '../models/credentials';
import redis from '../models/redis';
import socket from '../models/socket';
import Room from '../models/room';

/**
 * Get All Room Keys Where tokens are expiring due the next 15mins
 */
const getExpiringRoomCredentials = (): Promise<string[]> =>
  new Promise((resolve, reject) =>
    redis.zrangebyscore(
      'credentials:expirations',
      Date.now(),
      Date.now() + 15 * 60 * 1000,
      (err, res) => (err ? reject(err) : resolve(res))
    )
  );

const getExpiringRooms = (): Promise<string[]> =>
  new Promise((resolve, reject) =>
    redis.zrangebyscore(
      'room:expirations',
      Date.now(),
      Date.now() + 15 * 60 * 1000,
      (err, res) => (err ? reject(err) : resolve(res))
    )
  );

const getExpiredRooms = (): Promise<string[]> =>
  new Promise((resolve, reject) =>
    redis.zrangebyscore('room:expirations', 0, Date.now(), (err, res) =>
      err ? reject(err) : resolve(res)
    )
  );

export default schedule('0 */5 * * * *', async () => {
  // refresh access_token
  try {
    const credentials = await getExpiringRoomCredentials();

    await Promise.all(
      credentials.map(async id => {
        const credential = new Credentials(id);
        await credential.init();
        await credential.refreshAccessToken();
      })
    );
  } catch (e) {
    console.error(e);
  }

  // ask if the admin is still alive
  try {
    const rooms = await getExpiringRooms();

    rooms.forEach(
      room =>
        socket.emitToRoom(`${room}:admin`, 'room:expiries_soon', undefined) // inform the client
    );
  } catch (e) {
    console.error(e);
  }

  // delete all expired rooms
  try {
    const rooms = await getExpiredRooms();

    rooms.forEach(roomId => {
      const room = new Room(roomId);
      room.destroy();
    });
  } catch (e) {
    console.error(e);
  }
});

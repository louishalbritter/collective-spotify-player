import { uniqBy } from 'lodash';
import SpotifyWebApi from 'spotify-web-api-node';
import Voter from './voter';

class PlayList {
  id: string;

  private spotifyClient: SpotifyWebApi;

  constructor(id: string, spotifyClient: SpotifyWebApi) {
    this.id = id;
    this.spotifyClient = spotifyClient;
  }

  async getSnapshotId() {
    const {
      body: { snapshot_id: snapshotId },
    } = await this.spotifyClient.getPlaylist(this.id);

    return snapshotId;
  }

  private async getChunk(
    fields: (keyof SpotifyApi.TrackObjectFull)[],
    tracks: SpotifyApi.TrackObjectFull[] = []
  ): Promise<SpotifyApi.TrackObjectFull[]> {
    const {
      body: { items, next },
    } = await this.spotifyClient.getPlaylistTracks(this.id, {
      fields: `items(track(${fields.join(',')}))`,
    });

    if (next) {
      return this.getChunk(fields, [
        ...tracks,
        ...items.map(({ track }) => track),
      ]);
    }

    return [...tracks, ...items.map(({ track }) => track)];
  }

  async getAllSongs(fields: (keyof SpotifyApi.TrackObjectFull)[]) {
    return this.getChunk(fields);
  }
  
  async getAllSongsUnique(fields: (keyof SpotifyApi.TrackObjectFull)[]) {
    const tracks = await this.getChunk(fields);

    return uniqBy(tracks, 'id');
  }

  async getAllSongsWithVotes(
    fields: (keyof SpotifyApi.TrackObjectFull)[],
    uid?: string
  ) {
    const tracks = await this.getAllSongsUnique(fields);

    if (!uid) return tracks;

    return Promise.all(
      tracks.map(async track => {
        const voter = new Voter(this.spotifyClient, this, track.id, uid);
        return {
          ...track,
          vote: await voter.getVotingState(),
        };
      })
    );
  }
}

export default PlayList;

import SpotifyWebApi from 'spotify-web-api-node';
import { VoteDirection } from 'types';
import { shuffle, random } from 'lodash';
import axios from 'axios';
import { parseTrack, parseTracks } from '../controller/utils';
import Credentials from './credentials';
import Importer from './import';
import PlayList from './playlist';
import redis, { getAsync, setAsync } from './redis';
import spotify from './spotify';
import Voter from './voter';

class Room {
  id: string;

  private credentials: Credentials;

  private spotifyClient: SpotifyWebApi;

  playlist: PlayList;

  constructor(
    id: string,
    spotifyClient: SpotifyWebApi = spotify(),
    credentials = new Credentials(id, spotifyClient)
  ) {
    this.id = id;
    this.spotifyClient = spotifyClient;
    this.credentials = credentials;
    this.playlist = new PlayList(id, this.spotifyClient);
  }

  async init() {
    await this.credentials.init();
  }

  async import(uris: string[]) {
    const importer = new Importer(this.id, this.spotifyClient, this.playlist);
    return importer.importTracks(uris);
  }

  async voteTrack(
    sessionId: string,
    trackId: string,
    direction?: VoteDirection
  ) {
    const voter = new Voter(
      this.spotifyClient,
      this.playlist,
      trackId,
      sessionId
    );
    return voter.vote(direction);
  }

  async getTrack(trackId: string) {
    const { body } = await this.spotifyClient.getTrack(trackId);

    return parseTrack(body);
  }

  async play(deviceId: string) {
    const {
      body: {
        tracks: { total },
      },
    } = await this.spotifyClient.getPlaylist(this.id);

    await this.spotifyClient.play({
      context_uri: `spotify:playlist:${this.id}`,
      device_id: deviceId,
      offset: {
        // start at random position
        position: random(total),
      },
    });
  }

  /**
   * Get random track from playlist but avoid to play tracks multiple times in a row
   */
  private async getNextTrackUri(currentTrack: string) {
    const tracks = await this.playlist.getAllSongs(['uri']);

    const {
      body: { items: recentlyPlayedItems },
    } = await this.spotifyClient.getMyRecentlyPlayedTracks();

    const recentlyPlayed = [
      ...recentlyPlayedItems.map(({ track: { uri } }) => uri).reverse(),
      `spotify:track:${currentTrack}`,
    ];

    const [nextTrack] = tracks
      .map(({ uri }) => uri)
      .sort(a => (recentlyPlayed.includes(a) ? 1 : Math.random() - 0.5));

    return nextTrack;
  }

  private async planNextTrack(currentTrack: string) {
    const nextTrack = await this.getNextTrackUri(currentTrack);

    // coming soon: https://github.com/thelinmichael/spotify-web-api-node/pull/302
    // TODO: remove axios if spotify supports this function
    return axios.post(
      `https://api.spotify.com/v1/me/player/queue?uri=${nextTrack}`,
      {},
      {
        headers: { Authorization: `Bearer ${this.accessToken}` },
      }
    );
  }

  async getCurrentTrack() {
    return getAsync(`playlist:${this.id}:current`);
  }

  async setCurrentTrack(id: string) {
    await this.planNextTrack(id);
    return setAsync(`playlist:${this.id}:current`, id);
  }

  get accessToken() {
    return this.spotifyClient.getAccessToken();
  }

  async search(query: string, offset = 0) {
    const {
      body: { tracks },
    } = await this.spotifyClient.searchTracks(query, {
      offset,
    });

    return parseTracks(tracks?.items || []);
  }

  private async getSeeds() {
    const currentTracks = await this.playlist.getAllSongsUnique(['id']);
    if (currentTracks.length)
      return shuffle(currentTracks)
        .map(({ id }) => id)
        .slice(0, 5);

    const {
      body: { items },
    } = await this.spotifyClient.getMyRecentlyPlayedTracks({
      limit: 5,
    });

    return items.map(({ track: { id } }) => id);
  }

  async getRecommendations() {
    const seeds = await this.getSeeds();

    const {
      body: { tracks },
    } = await this.spotifyClient.getRecommendations({
      seed_tracks: seeds,
    });

    return parseTracks(tracks);
  }

  destroy() {
    redis.del(`*:${this.id}:*`);
    this.credentials.delete();
    redis.zrem('room:expirations', this.id);
  }

  keepAlive() {
    redis.zadd('room:expirations', Date.now() + 1000 * 60 * 60, this.id);
  }
}

export default Room;

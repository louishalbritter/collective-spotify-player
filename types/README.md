# Types

All typescript interfaces and types, which are shared between frontend and backend are here.
There are the definition of possible socket events and its payloads and return values as well. The return value is provided through the payload of the callback function.
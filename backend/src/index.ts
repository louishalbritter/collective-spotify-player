import { createServer } from 'http';
import './controller/cron';
import eventHandlers from './controller/events';
import socket from './models/socket';

const server = createServer();

socket.init(server, eventHandlers);

server.listen(3000, () => console.log('listening on *:3000'));

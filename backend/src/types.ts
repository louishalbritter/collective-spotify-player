import { Socket } from 'socket.io';
import { JWTPayload, ClientEvents } from 'types';
import Room from './models/room';

export type PublicEventHandler<
  E extends keyof ClientEvents['public']
> = (conn: {
  socket: Socket;
  jwtData?: JWTPayload;
  room?: Room;
  payload: ClientEvents['public'][E]['payload'];
}) => Promise<ClientEvents['public'][E]['return']>;

export type PrivateEventHandler<
  E extends keyof ClientEvents['private']
> = (conn: {
  socket: Socket;
  jwtData: JWTPayload;
  room: Room;
  payload: ClientEvents['private'][E]['payload'];
}) => Promise<ClientEvents['private'][E]['return']>;

export type EventHandlers = {
  public: {
    [E in keyof ClientEvents['public']]: PublicEventHandler<E>;
  };
  private: {
    [E in keyof ClientEvents['private']]: PrivateEventHandler<E>;
  };
};

import { createClient } from 'redis';
import { promisify } from 'util';

const redis = createClient({
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
});

redis.on('error', console.error);

export const getAsync = promisify(redis.get).bind(redis);
export const setAsync = promisify(redis.set).bind(redis);

export default redis;

import io from "socket.io-client";
import { ClientEvents, ServerEvents } from "types";
import { getJWT } from "./jwt";

const socket = io(
  process.env.NODE_ENV === "production"
    ? `wss://api.collective-listening.rocks`
    : "ws://localhost:3000",
  {}
);

type Events = ClientEvents["private"] & ClientEvents["public"];

/**
 * Send event and recive response
 * @param event
 * @param payload
 * \
 * Usage:
 * ```ts
 * const res = await emitEvent("room:join", payload)
 * ```
 */
export const emitEvent = <T extends keyof Events>(
  event: T,
  payload: Events[T]["payload"]
): Promise<Events[T]["return"]> =>
  new Promise((resolve, reject) =>
    socket.emit(
      event,
      getJWT(),
      payload,
      (err: string, res: Events[T]["return"]) =>
        err ? reject(err) : resolve(res)
    )
  );

/**
 * Listen to events from server
 * @param event
 * @param handler
 * \
 * Usage:
 * ```ts
 * listenToEvent("error", alert)
 * ```
 */
export const listenToEvent = <T extends keyof ServerEvents>(
  event: T,
  handler: (val: ServerEvents[T]) => void
): void => {
  socket.on(event, (val: ServerEvents[T]) => handler(val));
};

export default socket;

import SpotifyWebApi from 'spotify-web-api-node';
import redis, { getAsync } from './redis';
import spotify from './spotify';

interface CredentialsData {
  accessToken: string;

  refreshToken: string;
}
class Credentials {
  private id: string;

  private spotifyClient: SpotifyWebApi;

  private data?: CredentialsData;

  constructor(
    id: string,
    spotifyClient: SpotifyWebApi = spotify(),
    data?: CredentialsData
  ) {
    this.id = id;
    this.spotifyClient = spotifyClient;
    this.data = data;
  }

  save(expiries: number) {
    redis.set(`credentials:${this.id}`, JSON.stringify(this.data));
    redis.zadd(
      'credentials:expirations',
      Date.now() + expiries * 1000,
      this.id
    );
  }

  async init() {
    const credentials = await getAsync(`credentials:${this.id}`);
    if (typeof credentials === 'string') {
      this.data = JSON.parse(credentials);
    }
    if (this.data) {
      this.spotifyClient.setAccessToken(this.data?.accessToken);
      this.spotifyClient.setRefreshToken(this.data?.refreshToken);
    } else {
      throw Error('no Credentials found');
    }
  }

  async refreshAccessToken() {
    if (this.data) {
      const {
        body: { access_token: accessToken, expires_in: expiries },
      } = await this.spotifyClient.refreshAccessToken();

      this.data.accessToken = accessToken;
      this.save(expiries);
      return;
    }
    throw Error('no Credentials found');
  }

  delete() {
    redis.zrem('credentials:expirations', this.id);
    redis.del(`credentials:${this.id}`);
  }
}

export default Credentials;

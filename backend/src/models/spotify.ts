import SpotifyWebApi from 'spotify-web-api-node';

export default (): SpotifyWebApi =>
  new SpotifyWebApi({
    clientId: process.env.SPOTIFY_CLIENTID,
    clientSecret: process.env.SPOTIFY_CLIENTSECRET,
    redirectUri:
      process.env.NODE_ENV === 'production'
        ? `https://${process.env.HOST}/create`
        : 'http://localhost:8080/create',
  });

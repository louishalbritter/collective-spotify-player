import { JWTPayload } from "types";

export const setJWT = (jwt: string) => localStorage.setItem("jwt", jwt);
export const getJWT = () => localStorage.getItem("jwt");
export const getJWTPayload = (): JWTPayload | undefined => {
  const jwt = getJWT();

  if (jwt) {
    try {
      return JSON.parse(atob(jwt.split(".")[1]));
    } catch (e) {
      // ignore
    }
  }

  return undefined;
};

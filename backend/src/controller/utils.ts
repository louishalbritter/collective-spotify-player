import { Track } from 'types';

export const parseTrack = ({
  id,
  name,
  artists,
}: SpotifyApi.TrackObjectFull | SpotifyApi.TrackObjectSimplified): Track => ({
  id,
  name,
  artist: artists.map(a => a.name).join(', '),
});

export const parseTracks = (
  tracks: (SpotifyApi.TrackObjectFull | SpotifyApi.TrackObjectSimplified)[]
): Track[] => tracks.map(parseTrack);

import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Index from "../components/Index.vue";
import CreateRoom from "../components/CreateRoom.vue";
import Content from "../components/Content.vue";
import About from "../content/about.md";
import Privacy from "../content/privacy.md";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    component: Home,
    children: [
      {
        path: "/",
        component: Index,
        children: [
          {
            path: "/create",
            component: CreateRoom,
          },
        ],
      },
      {
        path: "/content",
        component: Content,
        children: [
          {
            path: "/about",
            component: About,
          },
          {
            path: "/privacy",
            component: Privacy,
          },
        ],
      },
    ],
  },
  {
    path: "/room/:id",
    name: "room",
    component: () => import(/* webpackChunkName: "room" */ "../views/Room.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

# Collective Listening

an open source tool to create a spotify playlist where user can vote tracks

## Develop

```
docker-compose -f "docker-compose.develop.yml" up
```

## Production

```
docker-compose up
```

## TODO

- improve error handling
- cleanup
- testing
- documentation

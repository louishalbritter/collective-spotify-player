import Credentials from '../models/credentials';
import Room from '../models/room';
import spotify from '../models/spotify';

export const createNewRoom = async (
  code: string,
  name: string
): Promise<Room> => {
  const spotifyClient = spotify();
  const {
    body: {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expires,
    },
  } = await spotifyClient.authorizationCodeGrant(code);

  spotifyClient.setAccessToken(accessToken);
  spotifyClient.setRefreshToken(refreshToken);

  const {
    body: { id: userId },
  } = await spotifyClient.getMe();

  const {
    body: { id },
  } = await spotifyClient.createPlaylist(userId, name, {
    public: false,
    description: 'created by https://collective-listening.rocks/',
  });

  const credentials = new Credentials(id, spotifyClient, {
    refreshToken,
    accessToken,
  });

  credentials.save(expires);

  const room = new Room(id, spotifyClient, credentials);

  room.keepAlive();

  return room;
};

export const getRoomById = async (id: string) => {
  const room = new Room(id);
  await room.init();

  return room;
};

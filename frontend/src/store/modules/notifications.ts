import { Module } from "vuex";
import { NotificationType } from "types";

interface Notification {
  type: NotificationType;
  message: string;
  key: number;
  value: boolean;
}

const notifications: Module<
  {
    notifications: Notification[];
  },
  unknown
> = {
  namespaced: true,
  state: {
    notifications: []
  },
  mutations: {
    error(state, message: string) {
      state.notifications = [
        ...state.notifications,
        {
          message,
          type: "error",
          key: Date.now(),
          value: true
        }
      ];
    },
    info(state, message: string) {
      state.notifications = [
        ...state.notifications,
        {
          message,
          type: "info",
          key: Date.now(),
          value: true
        }
      ];
    }
  }
};

export default notifications;

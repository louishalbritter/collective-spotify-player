import SpotifyWebApi from 'spotify-web-api-node';
import { VoteDirection } from 'types';
import PlayList from './playlist';
import redis, { getAsync, setAsync } from './redis';

class Voter {
  private spotifyClient: SpotifyWebApi;

  private playlist: PlayList;

  private trackId: string;

  private sessionId: string;

  constructor(
    spotifyClient: SpotifyWebApi,
    playlist: PlayList,
    trackId: string,
    sessionId: string
  ) {
    this.spotifyClient = spotifyClient;
    this.playlist = playlist;
    this.trackId = trackId;
    this.sessionId = sessionId;
  }

  private get redisKey(): string {
    return `vote:${this.playlist.id}:${this.trackId}:${this.sessionId}`;
  }

  async getVotingState(): Promise<VoteDirection | undefined> {
    try {
      const state = await getAsync(this.redisKey);
      if (state === 'UP' || state === 'DOWN') return state;
    } catch (e) {
      // ignore
    }
    return undefined;
  }

  private async setKey(direction?: VoteDirection) {
    return direction !== undefined
      ? setAsync(this.redisKey, direction.toString())
      : new Promise((resolve, reject) =>
          redis.del(this.redisKey, err => (err ? reject() : resolve()))
        );
  }

  private async downVoteTrack() {
    const tracks = await this.playlist.getAllSongsUnique(['id']);
    const track = tracks.find(({ id }) => id === this.trackId);
    if (track) {
      await this.spotifyClient.removeTracksFromPlaylistByPosition(
        this.playlist.id,
        [tracks.indexOf(track)],
        await this.playlist.getSnapshotId()
      );
    }
  }

  private async upVoteTrack() {
    return this.spotifyClient.addTracksToPlaylist(this.playlist.id, [
      `spotify:track:${this.trackId}`,
    ]);
  }

  async vote(direction?: VoteDirection) {
    const state = await this.getVotingState();

    let vote = 0;

    if (state === 'UP') vote -= 1;
    if (state === 'DOWN') vote += 1;

    if (direction === 'UP') vote += 1;
    if (direction === 'DOWN') vote -= 1;

    for (let i = 0; i < Math.abs(vote); i += 1) {
      // eslint-disable-next-line no-await-in-loop
      if (vote > 0) await this.upVoteTrack();
      // eslint-disable-next-line no-await-in-loop
      if (vote < 0) await this.downVoteTrack();
    }

    await this.setKey(direction);
  }
}

export default Voter;

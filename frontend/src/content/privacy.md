# TLDR

We do create a **unique Identifier** for your device and save your interactions on our database. But we do **not use any tracking, fingerprinting, evaluation** etc what could make you suspicious.

The Interactions saved on our database will be deleted when the room gets closed.

If you are the creator of the room, you should be aware that we have not only your interactions but also your **spotify credentials** saved, to be able to control your playlist.
We do not have any insights on what is spotify doing with your data..

# Technically explained

This tool is **open source**. So if you are skeptical, you always can have a look on the **source code**.

## Authorization

### Spotify Credentials

Before you create a room, we need the permission for your spotify account, to create a playlist. Therefor we use the [Spotify Authorization](https://developer.spotify.com/documentation/general/guides/authorization-guide/).

On creation, we get a **authorization code** from spotify and send it to our server, which exchanges this code to an **access token** and a **refresh token**. These Tokens are saved in our **Redis** database.

The **access token** is valid for a limited time only. When it's about to expire, we refresh the token on our server.

The credentials are saved for the complete room. So if someone else is doing an interaction inside your room, your credentials will be used.

### Identifier

To make it a little bit harder for your users to cheat, we create a **unique identifier** for each client, that joins the room. It's stored as a **JSON Web Token** in the **LocalStorage** of your Browser. This contains not only the identifier, but whether you're the admin of the room or not.

The identifier is only used, to prevent a client to vote the same track multiple times. We do **not** use this for fingerprinting or whatever. We do also **not** collect or save any further sensitive data from you. But keep in mind, that we use the Web API from spotify. So we have no control on what spotify is doing with your data.

## Duration of your data stored in our database

We store the data in a **Redis** database. This is not designed to have data persistent over a long period of time.
When the room expires, we delete all data, that we have stored to this room, including the data of all clients that have been involved.

The room expires about **one hour** after the admin **disconnects** (and not reconnecting again) to our server.

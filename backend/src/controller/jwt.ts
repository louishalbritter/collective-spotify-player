import { sign, verify } from 'jsonwebtoken';
import { JWTPayload } from 'types';
import { v4 as uuid } from 'uuid';
import Room from '../models/room';

const secret = process.env.JWT_SECRET || '';

export const signJWT = (roomId: string, admin = false) =>
  sign(
    {
      roomId,
      admin,
      uid: uuid(),
    },
    secret,
    { expiresIn: 60 * 60 * 24 } // one day
  );

export const decodeJWT = (jwt: string) =>
  verify(jwt, secret) as JWTPayload | undefined;

export const jwtMiddleware = async (jwt?: string) => {
  if (!jwt) throw Error('not authenticated');

  const jwtData = decodeJWT(jwt);

  if (!jwtData) throw Error('not authenticated');

  const room = new Room(jwtData.roomId);
  await room.init();

  return {
    jwtData,
    room,
  };
};

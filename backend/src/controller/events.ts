import QRCode from 'qrcode';
import Room from '../models/room';
import socket from '../models/socket';
import {
  EventHandlers,
  PrivateEventHandler,
  PublicEventHandler,
} from '../types';
import { signJWT } from './jwt';
import { createNewRoom } from './room';
import { parseTracks } from './utils';

const createRoom: PublicEventHandler<'room:create'> = async ({
  payload: { code, name },
}) => {
  const room = await createNewRoom(code, name);

  return {
    id: room.id,
    jwt: signJWT(room.id, true),
  };
};

const joinRoom: PublicEventHandler<'room:join'> = async ({
  socket: socketClient,
  jwtData,
  payload: { id },
}) => {
  const room = new Room(id);
  await room.init();
  const tracks = await room.playlist.getAllSongsWithVotes(
    ['id', 'name', 'artists'],
    jwtData?.uid
  );

  socketClient.join(id);

  if (jwtData?.admin) socketClient.join(`${id}:admin`);

  return {
    room: {
      id,
      tracks: parseTracks(tracks),
      currentTrack: (await room.getCurrentTrack()) || undefined,
    },
    jwt: jwtData?.uid ? undefined : signJWT(id),
  };
};

const keepAlive: PrivateEventHandler<'room:keep_alive'> = async ({
  room,
  jwtData: { admin },
}) => {
  if (!admin) throw Error('not authenticated');
  room.keepAlive();
};

const voteTrack: PrivateEventHandler<'room:track:vote'> = async ({
  room,
  jwtData: { uid },
  payload: { id, vote },
}) => {
  await room.voteTrack(uid, id, vote);

  const track = await room.getTrack(id);

  if (vote) {
    socket.emitToRoom(room.id, 'room:track:voted', {
      track,
      vote,
    });
  }
};

const importTracks: PrivateEventHandler<'room:tracks:import'> = async ({
  room,
  payload: { uris },
}) => {
  const tracks = await room.import(uris);

  if (tracks.length) {
    socket.emitToRoom(room.id, 'room:tracks:added', parseTracks(tracks));
  }
};

const getRecommendations: PrivateEventHandler<'room:recommendations'> = async ({
  room,
}) => room.getRecommendations();

const startPlayer: PrivateEventHandler<'player:start'> = async ({
  room,
  jwtData: { admin },
  payload: { device },
}) => {
  if (!admin) throw Error('not authorized');

  return room.play(device);
};

const playerUpdated: PrivateEventHandler<'player:updated'> = async ({
  room,
  payload: id,
}) => {
  if (!room) throw Error('not authorized');
  await room.setCurrentTrack(id);
  socket.emitToRoom(room.id, 'player:updated', id);
};

const getAccessToken: PrivateEventHandler<'room:get:access_token'> = async ({
  room,
  jwtData: { admin },
}) => {
  if (!admin) throw Error('not authorized');

  const { accessToken } = room;

  if (!accessToken) throw Error('not authorized');

  return accessToken;
};

const search: PrivateEventHandler<'search'> = async ({
  room,
  payload: { offset, query },
}) => room.search(query, offset);

const getQrCode: PrivateEventHandler<'room:qrCode'> = async ({ room }) =>
  QRCode.toDataURL(`https://${process.env.HOST}/room/${room.id}`);

const eventHandlers: EventHandlers = {
  public: {
    'room:create': createRoom,
    'room:join': joinRoom,
  },
  private: {
    'room:tracks:import': importTracks,
    'room:qrCode': getQrCode,
    'room:track:vote': voteTrack,
    'room:recommendations': getRecommendations,
    'room:get:access_token': getAccessToken,
    'room:keep_alive': keepAlive,
    'player:start': startPlayer,
    'player:updated': playerUpdated,
    search,
  },
};

export default eventHandlers;

// Models

export type NotificationType = "error" | "info";

export type Track = {
  id: string;
  name: string;
  artist: string;
  vote?: VoteDirection;
};

export type Room = {
  /** Spotify playlist id */
  id: string;
  tracks: Track[];
  currentTrack?: string;
};

export type VoteDirection = "UP" | "DOWN" | undefined;

export type Vote = {
  vote?: VoteDirection;
  id: string;
};

export interface JWTPayload {
  roomId: string;
  admin: boolean;
  uid: string;
}

// Events

export type RoomCreate = {
  /** Spotify Authentication Code */
  code: string;
  /** Name of the Playlist / Room */
  name: string;
};

export type RoomJoin = {
  /** room id */
  id: string;
};

export type ClientEvents = {
  private: {
    "room:tracks:import": {
      payload: {
        uris: string[];
      };
      return: void;
    };
    "room:track:vote": {
      payload: Vote;
      return: void;
    };
    "room:recommendations": {
      payload: {};
      return: Track[];
    };
    "room:qrCode": {
      payload: {};
      return: string;
    };
    "room:keep_alive": {
      payload: {};
      return: void;
    };
    "room:get:access_token": {
      payload: {};
      return: string;
    };
    "player:start": {
      payload: {
        device: string;
      };
      return: void;
    };
    "player:updated": {
      payload: string;
      return: void;
    };
    search: {
      payload: {
        query: string;
        offset?: number;
      };
      return: Track[];
    };
  };
  public: {
    "room:create": {
      payload: RoomCreate;
      return: {
        id: string;
        jwt: string;
      };
    };
    "room:join": {
      payload: RoomJoin;
      return: {
        room: Room;
        jwt?: string;
      };
    };
  };
};

export type ServerEvents = {
  info: string;
  error: string;
  "room:track:voted": {
    track: Track;
    vote: VoteDirection;
  };
  "room:tracks:added": Track[];
  "room:expiries_soon": void;
  "player:updated": string;
};

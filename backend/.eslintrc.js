module.exports = {
  extends: ['airbnb-typescript', 'prettier'],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error'],
    'no-console': 'off',
    // yarn workspaces not supported.. might be solved with https://github.com/benmosher/eslint-plugin-import/pull/1696
    'import/no-extraneous-dependencies': 'off',
    '@typescript-eslint/indent': 'off'
  },
}

module.exports = {
  transpileDependencies: ["vuetify"],
  configureWebpack: {
    devtool: "source-map",
    module: {
      rules: [
        {
          test: /\.md$/,
          exclude: /(node_modules|bower_components)/,
          use: [
            "vue-loader",
            {
              loader: "markdown-to-vue-loader",
            },
          ],
        },
      ],
    },
  },
  pages: {
    index: {
      title: "Collective Listening",
      entry: "./src/main.ts",
    },
  },
  pwa: {
    name: "Collective Listening",
    themeColor: "#1DB954",
    msTileColor: "#191414",
    manifestOptions: {
      // eslint-disable-next-line @typescript-eslint/camelcase
      background_color: "#191414",
    },
  },
};

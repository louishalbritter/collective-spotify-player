import { match } from 'path-to-regexp';
import SpotifyWebApi from 'spotify-web-api-node';
import { uniqBy, flatten } from 'lodash';
import PlayList from './playlist';

class Importer {
  private id: string;

  private spotifyClient: SpotifyWebApi;

  private playlist: PlayList;

  private matchers = {
    track: match<{ id: string }>('/track/:id'),
    playlist: match<{ id: string }>('/playlist/:id'),
  };

  constructor(id: string, spotifyClient: SpotifyWebApi, playlist: PlayList) {
    this.id = id;
    this.spotifyClient = spotifyClient;
    this.playlist = playlist;
  }

  private async getTrack(id: string) {
    const {
      body: { name, artists },
    } = await this.spotifyClient.getTrack(id);

    return { id, name, artists } as SpotifyApi.TrackObjectSimplified;
  }

  private async getTracksByUrl(uri: string) {
    const url = new URL(uri);
    const trackMatch = this.matchers.track(url.pathname);
    const playlistMatch = this.matchers.playlist(url.pathname);

    // single track
    if (uri.startsWith('spotify:track')) {
      const [, , id] = uri.split(':');
      const track = await this.getTrack(id);
      return [track];
    }

    if (trackMatch !== false) {
      const track = await this.getTrack(trackMatch.params.id);
      return [track];
    }

    // complete playlist
    if (uri.startsWith('spotify:playlist')) {
      const [, , id] = uri.split(':');
      const playlist = new PlayList(id, this.spotifyClient);
      return playlist.getAllSongsUnique(['id', 'name', 'artists']);
    }
    if (playlistMatch !== false) {
      const playlist = new PlayList(
        playlistMatch.params.id,
        this.spotifyClient
      );
      return playlist.getAllSongsUnique(['id', 'name', 'artists']);
    }

    return [];
  }

  async importTracks(importUris: string[]) {
    const currentTracks = await this.playlist.getAllSongsUnique(['id']);
    const currentTrackIds = currentTracks.map(({ id }) => id);

    const tracks = uniqBy(
      flatten(
        await Promise.all(importUris.map(async uri => this.getTracksByUrl(uri)))
      ),
      'id'
    );

    const newTracks = tracks.filter(({ id }) => !currentTrackIds.includes(id));

    if (newTracks.length) {
      await this.spotifyClient.addTracksToPlaylist(
        this.id,
        newTracks.map(({ id }) => `spotify:track:${id}`)
      );
    }

    return newTracks;
  }
}

export default Importer;
